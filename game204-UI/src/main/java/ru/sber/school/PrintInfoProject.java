package ru.sber.school;

import org.apache.log4j.Logger;

import java.util.ResourceBundle;

public class PrintInfoProject {

    private static final Logger logger = Logger.getLogger(PrintInfoProject.class);

    public void printInfo() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("infoOfProject");
        ResourceBundle resourceBundleGit = ResourceBundle.getBundle("git");
        logger.info(resourceBundle.getString("groupId"));
        logger.info(resourceBundle.getString("artifactId"));
        logger.info(resourceBundle.getString("version"));
        logger.info("Git info : ");
        logger.info(resourceBundleGit.getString("git.commit.user.name"));
        logger.info(resourceBundleGit.getString("git.commit.id"));
    }
}

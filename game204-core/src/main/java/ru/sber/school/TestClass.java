package ru.sber.school;

import ru.sber.school.core.board.SquareBoard;

import java.util.ArrayList;
import java.util.List;

public class TestClass {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(25);
        for (int i = 0; i < 25; i++) {
            list.add(i);
        }
        SquareBoard<Integer> squareBoard = new SquareBoard<>(4);
        squareBoard.fillBoard(list);
    }

}

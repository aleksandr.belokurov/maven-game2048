package ru.sber.school.core;

import ru.sber.school.core.board.Board;
import ru.sber.school.core.board.Direction;
import ru.sber.school.core.utils.NotEnoughSpace;

public interface Game {
    void init();
    boolean canMove();
    boolean move(Direction direction);
    void addItem() throws NotEnoughSpace;
    Board getGameBoard();
    boolean hasWin();

}

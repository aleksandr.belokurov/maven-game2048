package ru.sber.school.core.board;

public enum Direction {
    RIGHT,
    LEFT,
    UP,
    BOTTOM
}
